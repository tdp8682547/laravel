import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: ['resources/css/app.css', 'resources/js/app.js',
            'resources/sass/todo.scss'
            ,'resources/js/pages/to-do-list.js'
        ],

            refresh: true,
        }),
    ],
});
