@extends('welcome')

@section('to-do-list')
    to-do-list | laravel 10
@endsection

@section('css')
@vite(['resources/sass/todo.scss'])
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
@endsection

@section('content')  
    <h1 class="px-lg-5 m-5 fs-1">To do list</h1>
    <div class="containers">
        <div id="newtask" class="d-flex">
            <input type="text" class="form-control" id="taskinfo" placeholder="Task to do ...">
            <button class="btn btn-outline-secondary"  id="add">Add</button>
        </div>
        <div id="tasklist"></div>
    </div>
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    @vite(['resources/js/pages/to-do-list.js'])
@endsection
